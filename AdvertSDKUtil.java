package com.autohome.advertlib.common.sdk.util;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.autohome.advertlib.common.util.AdvertAHSchemaParser;
import com.autohome.advertsdk.common.universalimageloader.IImageLoader;
import com.autohome.advertsdk.common.universalimageloader.IImageLoadingListener;
import com.autohome.advertsdk.common.util.AdvertSDKConfig;
import com.autohome.advertsdk.common.view.base.AdvertViewClickListener;
import com.autohome.mainlib.common.constant.AHClientConfig;
import com.autohome.mainlib.common.net.NetConstant;
import com.cubic.autohome.common.view.image.core.ImageLoader;
import com.cubic.autohome.common.view.image.core.assist.FailReason;
import com.cubic.autohome.common.view.image.core.listener.ImageLoadingListener;

/**
 * 广告SDK工具类
 */
public class AdvertSDKUtil {

    /**
     * 初始化广告sdk
     * 795上线
     */
    public static void initAdvertSDK(Application application) {
        //广告初始化配置
        AdvertSDKConfig.initContext(application);
        AdvertSDKConfig.sAppId = "2";
        AdvertSDKConfig.sAppVersion = AHClientConfig.getInstance().getAhClientVersion();
        AdvertSDKConfig.sDebug = AHClientConfig.getInstance().isDebug();
        AdvertSDKConfig.sPkgName = "com.cubic.autohome";
        AdvertSDKConfig.sUserAgent = NetConstant.USER_AGENT;
        AdvertSDKConfig.sAdvertViewClickListener = new AdvertViewClickListener() {
            @Override
            public void onClick(View view, String land) {
                if (view != null) {
                    Activity activity = (Activity) view.getContext();
                    AdvertAHSchemaParser.clickAction(activity, land);
                }
            }
        };
        AdvertSDKConfig.setImageLoader(new IImageLoader() {
            @Override
            public void displayImage(String uri, ImageView imageView, final IImageLoadingListener listener) {
                ImageLoader.getInstance().displayImage(uri, imageView, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {
                        listener.onLoadingStarted(s, view);
                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {
                        listener.onLoadingFailed(s, view);
                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        listener.onLoadingComplete(s, view, bitmap);
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {
                        listener.onLoadingCancelled(s, view);
                    }
                });
            }

            @Override
            public void cancelDisplayTask(ImageView imageView) {
                ImageLoader.getInstance().cancelDisplayTask(imageView);
            }
        });
    }
}
