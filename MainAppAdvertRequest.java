package com.autohome.advertlib.common.sdk.request;

import com.autohome.advertlib.common.net.ADRetryInterceptor;
import com.autohome.advertsdk.common.bean.AdvertResultBean;
import com.autohome.advertsdk.common.request.AdvertRequestUtil;
import com.autohome.mainlib.common.net.BaseServant;
import com.autohome.net.antihijack.interceptor.RetryInterceptor;
import com.autohome.net.core.ResponseListener;

import java.util.HashMap;

/**
 * 主软件广告请求接口
 */
public class MainAppAdvertRequest extends BaseServant<AdvertResultBean> {

    public MainAppAdvertRequest() {
        super();
    }

    /**
     * 请求广告数据
     *
     * @param requestParams
     * @param responseListener
     */
    public void getAdvertData(HashMap<String, String> requestParams, ResponseListener<AdvertResultBean> responseListener) {
        String url = AdvertRequestUtil.getUrl(requestParams);
        getData(url, responseListener);
    }

    @Override
    public AdvertResultBean parseData(String data) throws Exception {
        //data = "{\"result\":{\"list\":[{\"pvurls\":[\"http://leads.autohome.com.cn/landingpage/html/landPage1/id_2/\"],\"addata\":{\"info\":{\"pubtime\":\"2017-03-29\",\"seriesid\":\"145\"},\"title\":{\"src\":\"Polo大众\"},\"img\":{\"src\":\"https://adm3.autoimg.cn/admdfs/Adhtmlnet/g8/M14/3D/AD/wKjBz1jbhNCANOQjAAAOvOI21uo682.jpg\"},\"subtitle\":{\"src\":\"放大放大东方闪电\"},\"description\":{\"src\":\"1111111111111111111\"}},\"pvid\":\"d1db0d95-8150-4038-8781-2f81520c74cf0\",\"posindex\":1,\"rdpvurls\":[\"https://www.baidu.com?-pv-2117\"],\"jumptype\":1,\"land\":\"http://c.autohome.com.cn/adfront/click?ah_mark=60ECCA5789A74F1FB453CF4957BB261B05A9BBBB405355772C894F7A56EB5EB9AD2BAB9C62C7D7C043F1BBEE377F4A94E4E0E8B7C2F2E7FC99254F27150554AA83727917831E84E62CF09CE51C6BAF30FA3983360E1B0DBD334F0BA7BD28BE7E&u=85D10418E74DEA21EFA9C136D7BCE042D09F9F85F9900645DC4C21257BE3C8B525B88D279FE08662DC26472AC3CDC510D93282D169352636421DD0C47A2FE444241B0D066F2EC841033E0BC7312B900D&isjump=1\",\"links\":[\"\"],\"viewstyle\":11,\"areaid\":\"2117\",\"rdrqurls\":[\"https://www.baidu.com?-rq-2117\"]},{\"rdpvurls\":[\"https://www.baidu.com?-pv-2125\"],\"rdrqurls\":[\"https://www.baidu.com?-rq-2125\"],\"pvid\":\"300964316462306439352D383135302D343033382D383738312D3266383135323063373463663109312E3009333533323835303634383334393134090932313235090909093209\",\"areaid\":\"2125\",\"viewstyle\":0},{\"pvurls\":[\"https://www.sina.com.cn\"],\"addata\":{\"img\":{\"src\":\"https://adm3.autoimg.cn/admdfs/g10/M0F/3E/CB/wKgH0VjWhGKAf5PsAAAHxPn_a00427.jpg\"},\"title\":{\"src\":\"工行汇款给客户给\"},\"info\":{\"pubtime\":\"2017-03-29\"}},\"pvid\":\"d1db0d95-8150-4038-8781-2f81520c74cf5\",\"rdpvurls\":[\"http://rdx.autohome.com.cn/app/realdeliver?pvid=d1db0d95-8150-4038-8781-2f81520c74cf5\"],\"jumptype\":1,\"land\":\"http://c.autohome.com.cn/adfront/click?ah_mark=6C63B0DACCDBA2F3F5276CA76FFFF4CE05A9BBBB405355772C894F7A56EB5EB959691DF92B5A51AB06A04A84FF840AE0A29CB2C2CF3C23DD3F41B479D8A0FD99DE9991E0BAE3B5B665C0CCDF6BED2C73FA3983360E1B0DBD334F0BA7BD28BE7E&u=2AF2895DB81FF970BE1D9AB5B62D5798449882167B0C01C94BC51715F2EC4598\",\"viewstyle\":7,\"areaid\":\"2730\",\"rdrqurls\":[\"http://rdx.autohome.com.cn/app/realdeliver?pvid=d1db0d95-8150-4038-8781-2f81520c74cf5\"]},{\"rdpvurls\":[\"https://rdx.autohome.com.cn/app/realdeliver?pvid=300964316462306439352D383135302D343033382D383738312D3266383135323063373463663309312E3009333533323835303634383334393134090932363136090909093209\"],\"rdrqurls\":[\"https://rdx.autohome.com.cn/app/realdeliver?pvid=300964316462306439352D383135302D343033382D383738312D3266383135323063373463663309312E3009333533323835303634383334393134090932363136090909093209\"],\"pvid\":\"300964316462306439352D383135302D343033382D383738312D3266383135323063373463663309312E3009333533323835303634383334393134090932363136090909093209\",\"areaid\":\"2616\",\"viewstyle\":0},{\"rdpvurls\":[\"https://www.baidu.com?-pv-2126\"],\"rdrqurls\":[\"https://www.baidu.com?-rq-2126\"],\"pvid\":\"300964316462306439352D383135302D343033382D383738312D3266383135323063373463663209312E3009333533323835303634383334393134090932313236090909093209\",\"areaid\":\"2126\",\"viewstyle\":0},{\"pvurls\":[\"https://www.sina.com.cn\"],\"addata\":{\"img\":{\"src\":\"https://adm3.autoimg.cn/admdfs/g10/M0C/3E/BA/wKgH0VjWdJCAaF_JAAAeegVJoL4265.jpg\"},\"title\":{\"src\":\"naysa的2729\"},\"info\":{\"pubtime\":\"2017-03-29\"}},\"pvid\":\"d1db0d95-8150-4038-8781-2f81520c74cf4\",\"rdpvurls\":[\"http://rdx.autohome.com.cn/app/realdeliver?pvid=d1db0d95-8150-4038-8781-2f81520c74cf4\"],\"jumptype\":1,\"land\":\"http://c.autohome.com.cn/adfront/click?ah_mark=70932FE4C2458D361B7DD163947F2BFA05A9BBBB405355772C894F7A56EB5EB95A67638CEA3BF0F7860488D23625E4A47F702398F06D1184684C7A71600DB770DE9991E0BAE3B5B665C0CCDF6BED2C73FA3983360E1B0DBD334F0BA7BD28BE7E&u=95C9738B57923B3DEABAF06170C187B3FEAD21F6F280D25E2CE7A34C9A60BE5F&isjump=1\",\"links\":[\"\"],\"viewstyle\":21,\"areaid\":\"2729\",\"rdrqurls\":[\"http://rdx.autohome.com.cn/app/realdeliver?pvid=d1db0d95-8150-4038-8781-2f81520c74cf4\"]}]},\"returncode\":0,\"message\":\"ok\"}";
        AdvertResultBean resultBean = AdvertRequestUtil.parseData(data);
        return resultBean;
    }

    @Override
    public RetryInterceptor getRetryInterceptor() {
        return new ADRetryInterceptor();
    }

}
